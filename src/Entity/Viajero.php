<?php declare(strict_types = 1);

namespace App\Entity;

use App\Repository\ViajeroRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;




/**
 * @ORM\Entity(repositoryClass=ViajeroRepository::class)
 */
class Viajero
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255))
     */
    private $cedula;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha_nacimiento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telefono;

    /**
     * @ORM\OneToMany(targetEntity="ViajeroViaje", mappedBy="viajero")
     * @ORM\JoinColumn(name="viaje_id", referencedColumnName="id", onDelete="SET NULL")
     * @var ArrayCollection
     */
    protected $viajes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCedula(): ?string
    {
        return $this->cedula;
    }

    public function setCedula(string $cedula): self
    {
        $this->cedula = $cedula;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getFechaNacimiento(): ?string
    {
        return $this->fecha_nacimiento;
    }

    public function setFechaNacimiento(string $fecha_nacimiento): self
    {
        $this->fecha_nacimiento = $fecha_nacimiento;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     * @return Viaje[]
     */
    public function getViajes(): array
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        return array_map(static function (ViajeroViaje $viajeroViaje) use ($serializer) {
            $jsonData =  $serializer->serialize($viajeroViaje->getViaje(), 'json');
            return $serializer->decode($jsonData,'json');
        }, $this->viajes->toArray());
    }

    /** @codeCoverageIgnore
     * @param ViajeroViaje $viajeroViaje
     * @return Viajero
     */
    public function addViaje(ViajeroViaje $viajeroViaje): self
    {
        $this->viajes->add($viajeroViaje);

        return $this;
    }
}
