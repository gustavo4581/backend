<?php declare(strict_types = 1);

namespace App\Entity;

use App\Repository\ViajeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ViajeRepository::class)
 */
class Viaje
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codigo_viaje;

    /**
     * @ORM\Column(type="integer")
     */
    private $numero_plazas;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lugar_origen;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $destino;

    /**
     * @ORM\Column(type="integer")
     */
    private $precio;

    /**
     * @ORM\OneToMany(targetEntity="ViajeroViaje", mappedBy="viajero")
     *  @ORM\JoinColumn(name="viajero_id", referencedColumnName="viaje_id", onDelete="SET NULL")
     * @var ArrayCollection
     */
    protected $viajeros;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigoViaje(): ?string
    {
        return $this->codigo_viaje;
    }

    public function setCodigoViaje(string $codigo_viaje): self
    {
        $this->codigo_viaje = $codigo_viaje;

        return $this;
    }

    public function getNumeroPlazas(): ?int
    {
        return $this->numero_plazas;
    }

    public function setNumeroPlazas(int $numero_plazas): self
    {
        $this->numero_plazas = $numero_plazas;

        return $this;
    }

    public function getLugarOrigen(): ?string
    {
        return $this->lugar_origen;
    }

    public function setLugarOrigen(string $lugar_origen): self
    {
        $this->lugar_origen = $lugar_origen;

        return $this;
    }

    public function getDestino(): ?string
    {
        return $this->destino;
    }

    public function setDestino(string $destino): self
    {
        $this->destino = $destino;

        return $this;
    }

    public function getPrecio(): ?int
    {
        return $this->precio;
    }

    public function setPrecio(int $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     * @return Viajero[]
     */
    public function getViajeros(): array
    {
        return array_map(static function (ViajeroViaje $viajeroViaje) {
            return $viajeroViaje->getViajero();
        }, $this->viajeros->toArray());
    }

}
