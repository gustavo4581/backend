<?php declare(strict_types = 1);

namespace App\Entity;

use App\Entity\Viaje;
use App\Entity\Viajero;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(
 *     name="viajero_viajes",
 *     uniqueConstraints={@UniqueConstraint(name="viajero_viaje_index", columns={"viajero_id", "viaje_id"})}
 *     )
 * @codeCoverageIgnore
 */
class ViajeroViaje
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Viajero", inversedBy="viajes")
     * @ORM\JoinColumn(nullable=false)
     * @var Viajero
     */
    protected $viajero;

    /**
     * @ORM\ManyToOne(targetEntity="Viaje", inversedBy="viajeros")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @var Viaje
     */
    protected $viaje;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @var DateTimeImmutable|null
     */
    protected $created;

    public function getId(): int
    {
        return $this->id;
    }

    public function getViajero(): Viajero
    {
        return $this->viajero;
    }

    public function setViajero(Viajero $viajero): self
    {
        $this->viajero = $viajero;

        return $this;
    }

    public function getViaje(): Viaje
    {
        return $this->viaje;
    }

    public function setViaje(Viaje $viaje): self
    {
        $this->viaje = $viaje;

        return $this;
    }

    public function getCreated(): ?DateTimeImmutable
    {
        return $this->created;
    }

    /** @ORM\PrePersist() */
    public function setCreatedValue(): self
    {
        $this->created = new DateTimeImmutable();

        return $this;
    }
}
