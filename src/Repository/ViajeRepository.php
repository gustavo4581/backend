<?php

namespace App\Repository;

use App\Entity\Viaje;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Viaje|null find($id, $lockMode = null, $lockVersion = null)
 * @method Viaje|null findOneBy(array $criteria, array $orderBy = null)
 * @method Viaje[]    findAll()
 * @method Viaje[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajeRepository extends ServiceEntityRepository
{
    private $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Viaje::class);
        $this->entityManager = $entityManager;
    }

    public function store(
        string $codigo_viaje,
        int $numero_plazas,
        string $lugar_origen,
        string $destino,
        int $precio
    ): Viaje
    {
        $newViaje = new Viaje();

        $newViaje
            ->setCodigoViaje($codigo_viaje)
            ->setNumeroPlazas($numero_plazas)
            ->setLugarOrigen($lugar_origen)
            ->setDestino($destino)
            ->setPrecio($precio);

        $this->entityManager->persist($newViaje);
        $this->entityManager->flush();

        return $newViaje;
    }

    public function updateViaje(Viaje $viaje): Viaje
    {
        $this->entityManager->persist($viaje);
        $this->entityManager->flush();

        return $viaje;
    }

    public function deleteViaje(Viaje $viaje): void
    {
        $this->entityManager->remove($viaje);
        $this->entityManager->flush();
    }
}
