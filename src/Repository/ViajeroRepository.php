<?php declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Viajero;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Viajero|null find($id, $lockMode = null, $lockVersion = null)
 * @method Viajero|null findOneBy(array $criteria, array $orderBy = null)
 * @method Viajero[]    findAll()
 * @method Viajero[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajeroRepository extends ServiceEntityRepository
{
    private $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Viajero::class);
        $this->entityManager = $entityManager;
    }

    public function store(string $cedula, string $nombre, string $fechaNacimiento, string $telefono): Viajero
    {
        $newViajero = new Viajero();

        $newViajero
            ->setCedula($cedula)
            ->setNombre($nombre)
            ->setFechaNacimiento($fechaNacimiento)
            ->setTelefono($telefono);

        $this->entityManager->persist($newViajero);
        $this->entityManager->flush();

        return $newViajero;
    }

    public function updateViajero(Viajero $viajero): Viajero
    {
        $this->entityManager->persist($viajero);
        $this->entityManager->flush();

        return $viajero;
    }

    public function deleteViajero(Viajero $viajero): void
    {
        $this->entityManager->remove($viajero);
        $this->entityManager->flush();
    }
}
