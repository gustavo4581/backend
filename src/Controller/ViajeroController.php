<?php declare(strict_types = 1);

namespace App\Controller;

use App\Repository\ViajeroRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ViajeroConstroller
 * @package App\Controller
 *
 * @Route("/api/")
 */
class ViajeroController extends AbstractController
{
    /**
     * @var ViajeroRepository
     */
    private $viajeroRepository;

    /**
     * ViajeroController constructor.
     * @param ViajeroRepository $viajeroRepository
     */
    public function __construct(ViajeroRepository $viajeroRepository )
    {
        $this->viajeroRepository = $viajeroRepository;
    }

    /**
     * @Route("viajero", name="add_viajero", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \JsonException
     */
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $cedula = $data['cedula'];
        $nombre = $data['nombre'];
        $fechaNacimiento = $data['fecha_nacimiento'];
        $telefono = $data['telefono'];

        if (!$cedula) {
            throw new NotFoundHttpException('Parametro requerido');
        }

        $newViajero = $this->viajeroRepository->store($cedula, $nombre, $fechaNacimiento, $telefono);

        return new JsonResponse(
            $newViajero,
            Response::HTTP_OK,
        );
    }

    /**
     * @Route("viajero/{id}", name="viajero", methods={"GET"})
     * @param int $id
     * @return JsonResponse
     */
    public function read(int $id): JsonResponse
    {
        $viajero = $this->viajeroRepository->findOneBy(['id' => $id]);

        $data = [
            'id' => $viajero->getId(),
            'cedula' => $viajero->getCedula(),
            'nombre' => $viajero->getNombre(),
            'fecha_nacimiento' => $viajero->getFechaNacimiento(),
            'telefono' => $viajero->getTelefono(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     *
     * @Route("viajeros", name="viajeros_todos", methods={"GET"})
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $viajeros = $this->viajeroRepository->findAll();
        $data = [];
        foreach ($viajeros as $viajero) {
            $data[] = [
                'id' => $viajero->getId(),
                'cedula' => $viajero->getCedula(),
                'nombre' => $viajero->getNombre(),
                'fecha_nacimiento' => $viajero->getFechaNacimiento(),
                'telefono' => $viajero->getTelefono(),
                'viajes' => $viajero->getViajes(),
            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("viajero/{id}", name="viajero_update", methods={"PUT"})
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     * @throws \JsonException
     */
    public function update(int $id, Request $request): JsonResponse
    {
        $viajero = $this->viajeroRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        empty($data['cedula']) ? true : $viajero->setCedula($data['cedula']);
        empty($data['nombre']) ? true : $viajero->setNombre($data['nombre']);
        empty($data['fecha_nacimiento']) ? true : $viajero->setFechaNacimiento($data['fecha_nacimiento']);
        empty($data['telefono']) ? true : $viajero->setTelefono($data['telefono']);

        $updateViajero =$this->viajeroRepository->updateViajero($viajero);

        return new JsonResponse(
        ['status' => 'Viajero actualizado!'],
            Response::HTTP_OK
        );
    }

    /**
     * @Route("viajero/{id}", name="viajero_delete", methods={"DELETE"})
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $viajero = $this->viajeroRepository->findOneBy(['id' => $id]);

        $this->viajeroRepository->deleteViajero($viajero);

        return new JsonResponse(['status' => 'Viajero borrado'], Response::HTTP_OK);
    }
}
