<?php declare(strict_types = 1);

namespace App\Controller;

use App\Repository\ViajeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ViajeConstroller
 * @package App\Controller
 *
 * @Route("/api/")
 */
class ViajeController extends AbstractController
{
    /**
     * @var ViajeRepository
     */
    private $viajeRepository;

    /**
     * ViajeController constructor.
     * @param ViajeRepository $viajeRepository
     */
    public function __construct(ViajeRepository $viajeRepository )
    {
        $this->viajeRepository = $viajeRepository;
    }

    /**
     * @Route("viaje", name="add_viaje", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \JsonException
     */
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $codigoViaje = $data['codigo_viaje'];
        $numeroPlazas = (int)$data['numero_plazas'];
        $lugar_origen = $data['lugar_origen'];
        $destino = $data['destino'];
        $precio = (int)$data['precio'];

        if (!$codigoViaje) {
            throw new NotFoundHttpException('Parametro requerido');
        }

        $newViaje = $this->viajeRepository->store(
            $codigoViaje,
            $numeroPlazas,
            $lugar_origen,
            $destino,
            $precio
        );

        return new JsonResponse(
            $newViaje,
            Response::HTTP_OK,
        );
    }

    /**
     * @Route("viaje/{id}", name="viaje", methods={"GET"})
     * @param int $id
     * @return JsonResponse
     */
    public function read(int $id): JsonResponse
    {
        $viaje = $this->viajeRepository->findOneBy(['id' => $id]);

        if (!$viaje) {
            throw new NotFoundHttpException('No existe el viaje');
        }

        $data = [
            'id' => $viaje->getId(),
            'codigo_viaje' => $viaje->getCodigoViaje(),
            'numero_plazas' => $viaje->getNumeroPlazas(),
            'lugar_origen' => $viaje->getLugarOrigen(),
            'destino' => $viaje->getDestino(),
            'precio' => $viaje->getPrecio(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("viajes", name="viajes_todos", methods={"GET"})
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $viajes = $this->viajeRepository->findAll();
        $data = [];
        foreach ($viajes as $viaje) {
            $data[] = [
                'id' => $viaje->getId(),
                'codigo_viaje' => $viaje->getCodigoViaje(),
                'numero_plazas' => $viaje->getNumeroPlazas(),
                'lugar_origen' => $viaje->getLugarOrigen(),
                'destino' => $viaje->getDestino(),
                'precio' => $viaje->getPrecio(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("viaje/{id}", name="viaje_update", methods={"PUT"})
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     * @throws \JsonException
     */
    public function update(int $id, Request $request): JsonResponse
    {
        $viaje = $this->viajeRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        empty($data['codigo_viaje']) ? true : $viaje->setCodigoViaje($data['codigo_viaje']);
        empty($data['numero_plazas']) ? true : $viaje->setNumeroPlazas((int)$data['numero_plazas']);
        empty($data['lugar_origen']) ? true : $viaje->setLugarOrigen($data['lugar_origen']);
        empty($data['destino']) ? true : $viaje->setDestino($data['destino']);
        empty($data['precio']) ? true : $viaje->setPrecio((int)$data['precio']);

        $updateViaje = $this->viajeRepository->updateViaje($viaje);

        return new JsonResponse(
            ['status' => 'Viaje actualizado!'],
            Response::HTTP_OK
        );
    }

    /**
     * @Route("viaje/{id}", name="viaje_delete", methods={"DELETE"})
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $viaje = $this->viajeRepository->findOneBy(['id' => $id]);

        $this->viajeRepository->deleteViaje($viaje);

        return new JsonResponse(['status' => 'Viaje borrado'], Response::HTTP_OK);
    }

}
