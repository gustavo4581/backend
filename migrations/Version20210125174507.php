<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210125174507 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE viajero_viajes (id INT AUTO_INCREMENT NOT NULL, viajero_id INT NOT NULL, viaje_id INT NOT NULL, created DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_20342A63FAF4CFE9 (viajero_id), INDEX IDX_20342A6394E1E648 (viaje_id), UNIQUE INDEX viajero_viaje_index (viajero_id, viaje_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE viajero_viajes ADD CONSTRAINT FK_20342A63FAF4CFE9 FOREIGN KEY (viajero_id) REFERENCES viajero (id)');
        $this->addSql('ALTER TABLE viajero_viajes ADD CONSTRAINT FK_20342A6394E1E648 FOREIGN KEY (viaje_id) REFERENCES viaje (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE viajero_viajes');
    }
}
